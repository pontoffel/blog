---
title: ! 'Some Tips For Careless Programmers'
layout: post
categories:
- mistakes
- prevention
---

If you're like me, then you probably make a lot of mistakes. Any time I don't meticulously plan every single detail of my code then implement it as precisely and single-mindedly as possible, I always end up making a mistake or two dozen. Here are some of the strategies that I have personally used to help prevent and mitigate the damage caused by my mind when it wanders.

## 1. Meticulously plan every single detail of your code then implement it as precisely and single-mindedly as possible
I didn't put this in my introduction for nothing; it works like a charm. Unfortunately, it also takes forever.

## 2. Mark any code you've written that you aren't completely sure about
Maybe you've quickly typed a couple of nice lines, but you aren't 100% sure that this and that will really turn out to be what you're expecting it to be. You should definitely mark it, then. Personally, I mark my suspect code with a comment like this: 

{% highlight java %}
// TODO: Check this
{% endhighlight %}

I use ``TODO:`` in particular because in every code editor I use, it is highlighted.

## 3. Mark any code that you're using to fix a mistake
Just a few minutes ago, I was almost finished working on a problem. I was going over my code, trying to figure out why it wasn't completely working. Eventually, I stumbled across a piece of code that I didn't really know the purpose of and didn't want to figure out (3 AM does not make thinking good), so I just straight-up removed it. As soon as I did so, everything worked perfectly. I could have gotten this over with way sooner had I just marked this code previously, as it was code that I put in as part of an attempt to solve an earlier problem, but forgot to remove after it had become irrelevant.
